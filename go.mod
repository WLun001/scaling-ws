module scaling-ws

go 1.15

require (
	github.com/avast/retry-go v2.6.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/gorilla/websocket v1.4.2
	github.com/nats-io/nats-server/v2 v2.1.8 // indirect
	github.com/nats-io/nats.go v1.10.0
	github.com/stretchr/testify v1.6.1 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)
